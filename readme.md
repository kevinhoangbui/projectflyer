## Project Flyer

The theme of Project Flyer is to present the latest properties (real estates) for sale. You may register, post and manage your flyers (advertisements).

This project is developed implementing Laravel 5.2, Twitter Bootstrap, Dropzone.js and SweetAlert. It will be continuously updated and improved.

Visit the website at http://82.196.8.243/. You may login with the following credentials: email: minhtien.swin@gmail.com, password: password. You may register to become a member and post flyers, too.
