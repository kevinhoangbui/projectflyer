<?php

use App\Flyer;
use App\User;
use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class FlyerTest extends TestCase{

	use DatabaseTransactions;
	protected $flyer;

	public function setUp(){
		parent::setUp();

		$faker = Factory::create();
		$this->flyer = new Flyer([
			'address' => $faker->address,
			'city' => $faker->city,
			'zip' => $faker->postcode,
			'state' => $faker->state,
			'country' => $faker->country,
			'price' => $faker->numberBetween(500000, 1000000),
			'description' => $faker->paragraph(8)
		]);

	}


	/** @test */
	function all_flyers_could_be_listed(){
		//given
		factory(App\User::class, 10)->create();
		factory(App\Flyer::class, 50)->create();

		//when
		$flyers = Flyer::all();

		//then
		$this->assertEquals(50, $flyers->count());
	}

	/** @test */
	function a_user_can_view_all_of_their_own_flyers(){
		//given
		$users = factory(App\User::class, 2)->create();
		factory(App\Flyer::class, 13)->create([
			'user_id' => 2
		]);

		//when
		$user = $users[1];
		$flyers = $user->flyers()->get();

		//then
		$this->assertEquals(13, $flyers->count());
	}

	/** @test */
	function a_user_is_able_to_create_a_new_flyer(){
		//given
		$user = factory(App\User::class, 1)->create();

		//when
		$user->flyers()->save($this->flyer);

		//then
		$this->assertEquals(1, $user->flyers()->first()->user_id);

	}

	/** @test */
	function check_whether_a_flyer_is_owned_by_a_user(){
		//given
		$users = factory(User::class, 2)->create();

		//when
		$users[0]->flyers()->save($this->flyer);

		//then
		$this->assertTrue($this->flyer->ownedBy($users[0]));
		$this->assertFalse($this->flyer->ownedBy($users[1]));
	}

	/** @test */
	function the_price_of_a_flyer_could_be_correctly_formatted(){
		//given
		$user = factory(User::class)->create();
		$flyer = factory(Flyer::class)->create([
			'user_id' => $user->id,
			'price' => 500000
		]);

		//then
		$this->assertEquals('$500,000', $flyer->price);
	}
}