<?php
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase{
	
	use DatabaseTransactions;

	protected $newUser;

	public function setUp(){
		parent::setUp();

		//given
		$name = 'Jason Williams';
		$email = 'jason.williams@gmail.com';
		$password = bcrypt('1234567');

		//when
		User::create(compact('name', 'email', 'password'));
		//get the last user from database (the one that has just been created)
		$this->newUser = User::all()->last();
	}

	/** @test */
	function a_user_is_able_to_register(){
		//then
		$this->assertEquals('Jason Williams', $this->newUser->name);
	}

}