<?php

namespace App;

use Mockery;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use TestCase;

class AddPhotoToFlyerTest extends TestCase{

	use DatabaseTransactions;

	/** @test */
	function it_processes_a_form_to_add_a_photo_to_a_flyer(){
		$user = factory(User::class)->create();
		$flyer = factory(Flyer::class)->create([
			'user_id' => $user->id
		]);

		$file = Mockery::mock(UploadedFile::class, [
			'getClientOriginalName' => 'Foo',
			'getClientOriginalExtension' => 'jpg'
		]);

		$file->shouldReceive('move')
			->once()
			->with('flyers_stuff/photos', 'NowFoo.jpg');

		$thumbnail = Mockery::mock(Thumbnail::class);

		$thumbnail->shouldReceive('make')
				->once()
				->with('flyers_stuff/photos/NowFoo.jpg', 'flyers_stuff/photos/tn-NowFoo.jpg');

		$form = new AddPhotoToFlyer($flyer, $file, $thumbnail);

		$form->save();

		$this->assertCount(1, $flyer->photos()->get());

	}
	
}

function time(){
	return 'Now';
}

function sha1($path){
	return $path;
}