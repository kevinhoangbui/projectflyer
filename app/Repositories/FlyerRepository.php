<?php

namespace App\Repositories;

use App\Flyer;
use App\User;

class FlyerRepository{
	
	/**
	 * Retrieve that latest flyers from all users with the default pagination of 9 records/page
	 * 
	 * @param  integer $perPage
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getLatestPaginated($perPage = 9){
		return Flyer::latest()->paginate($perPage);
	}

	/**
	 * Save a new flyer to the database
	 * 
	 * @param  App\User $user the user who created the flyer
	 * @param  App\Flyer $newFlyer the new flyer
	 * @return App\Flyer
	 */
	public function publishFlyer(User $user, Flyer $newFlyer){
		$flyer = $user->publish($newFlyer);
		return $flyer;
	}

	/**
	 * Get a specific flyer's details with its id
	 * @param  integer $userId
	 * @return App\Flyer
	 */
	public function findFlyer($flyerId){
		return Flyer::findOrFail($flyerId);
	}

	/**
	 * Retrieve all the flyers that belong to a specific user
	 * 
	 * @param  App\User $user
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function getFlyersForUser(User $user, $perPage = 5){
		return $user->flyers()->latest()->paginate($perPage);
	}
}